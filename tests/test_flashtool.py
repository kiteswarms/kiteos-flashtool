#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

from kiteos_flashtool import kiteos_flashtool


def test_firmware_flasher():
    puli_port = 12345
    puli_ttl = 0
    board_id = "Unknown_PCB"
    interface_address = "0.0.0.0"
    program_interface_output = False
    firmware_flasher = kiteos_flashtool.FirmwareFlasher(puli_port, puli_ttl,
                                                        interface_address, None, None)
    assert firmware_flasher
