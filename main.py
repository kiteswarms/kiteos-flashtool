# !/bin/python3

# Copyright (C) 2020 Kiteswarms GmbH - All Rights Reserved
#
# main.py
#
# Jan Lehmann    jan@kiteswarms.com
#
# https://www.kiteswarms.com

import sys
sys.path.insert(0, "src")
from kiteos_flashtool.kiteos_flashtool import main

if __name__ == "__main__":
    main()
