# !/bin/python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of kiteos-flashtool.
#
#     kiteos-flashtool is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     kiteos-flashtool is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with kiteos-flashtool.  If not, see <https://www.gnu.org/licenses/>.


from setuptools import setup, find_packages

setup(
    name="kiteswarms-kiteos-flashtool",
    author="Jan Lehmann",
    author_email="jan@kiteswarms.com",
    description="Tool to flash KiteOS hardware via ethernet.",
    long_description="More detailed description",
    url="https://code.kiteswarms.com/kiteswarms/kiteos-flashtool",
    install_requires=[
        "importlib_metadata",
        "kiteswarms-pulicast ~= 1.0",
        "kiteswarms-kitecom",
        "tqdm",
    ],
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    package_dir={"": "src"},
    packages=find_packages("src"),
    entry_points={
        "console_scripts": [
            "kiteos-flashtool = kiteos_flashtool.kiteos_flashtool:main",
            "kiteos-reset-tool = kiteos_flashtool.kiteos_reset_tool:main"
        ],
    },
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
