# KiteOS Flash Tool


The KiteOS flash tool is a command line tool used to write firmware- and bootloader binaries to the microcontrollers flash memory of KiteOS hardware via Ethernet.
The package also includes a tool used to reset the microcontroller of a board by software.

## Documentation

Documentation is available at https://kiteswarms.gitlab-pages.kiteswarms.com/kiteos-flashtool/

## Installation

```
git clone git@gitlab.com:kiteswarms/kiteos-flashtool.git
cd kiteos-flashtool
pip install .
```

## Usage

### Flash Tool

The KiteOS flash tool is executed by `kiteos-flashtool`.

The default `PULI_PORT` is `8765` and `PULI_TTL` is `1` (which should work in almost every case).
The `board_id` defines the ID of the board to flash, which could be for example `Nucleus-1.1.0#3`.
The  `file_path` is the path of the binary that should be flashed.
Make sure to flash bootloader binaries to the bootloader section and firmware binaries to the firmware section.

### Reset Tool

The KiteOS reset tool is executed by `kiteos-reset-tool`.

The default `PULI_PORT` is `8765` and `PULI_TTL` is `1` (which should work in almost every case).
The `board_id` defines the ID of the board to reset, which could be for example `Nucleus-1.1.0#3`.
