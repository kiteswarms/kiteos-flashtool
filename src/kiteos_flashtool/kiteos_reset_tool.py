# !/usr/bin/python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of kiteos-flashtool.
#
#     kiteos-flashtool is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     kiteos-flashtool is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with kiteos-flashtool.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import pulicast
from .kiteos_flashtool import FirmwareFlasher, CommunicationError


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description="Resets the firmware of a KiteOS hardware "
                                                 "component.")
    parser.add_argument("board_id", help="Target board unique hardware identifier.")
    parser.add_argument("-p", "--puli-port", default=pulicast.DEFAULT_PORT,
                        help="The pulicast port to publish on.")
    parser.add_argument("-t", "--puli-ttl", default=1,
                        help="The pulicast ttl to publish with.")
    parser.add_argument("-i", "--interface", default="0.0.0.0", type=str,
                        help="Pulicast capture interface ip address")
    args = parser.parse_args()

    # Reset firmware using the firmware flasher class
    firmware_flasher = FirmwareFlasher(args.puli_port, args.puli_ttl, args.interface, None, None)
    firmware_flasher.set_board_identifier(args.board_id)

    try:
        firmware_flasher.reset()
    except (CommunicationError) as e:
        print(e)
        exit(1)
