#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of kiteos-flashtool.
#
#     kiteos-flashtool is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     kiteos-flashtool is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with kiteos-flashtool.  If not, see <https://www.gnu.org/licenses/>.

from importlib_metadata import version, PackageNotFoundError

try:
    __version__ = version("kiteswarms-{}".format(__name__))
except PackageNotFoundError:
    # package is not installed
    raise
