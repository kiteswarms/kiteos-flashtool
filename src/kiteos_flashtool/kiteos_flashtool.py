#!/usr/bin/python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of kiteos-flashtool.
#
#     kiteos-flashtool is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     kiteos-flashtool is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with kiteos-flashtool.  If not, see <https://www.gnu.org/licenses/>.

import time
import struct
import argparse
import queue
import ctypes
import tqdm

import zcom

import pulicast
from pulicast import ThreadedNode, Address
from pulicast.discovery.node_discoverer import NodeDiscoverer
from pulicast.discovery.node_view import NodeView

# Message structure:
#
# Bytes:          0 - 22        23 - 45            46           47 - n
#              ------------------------------------------------------------
# Description: | Source ID | Destination ID | Message Type | Message Data |
#              ------------------------------------------------------------
#
# Embedded hardware ID:
#
# Bytes:          0 - 15      16 - 18    19 - 22
#              -----------------------------------
# Description: | PCB Type | PCB Version | PCB ID |
#              -----------------------------------

# Message definitions
STATUS = 0
ERASE_COMMAND = 1
ERASE_RESPONSE = 2
WRITE_COMMAND = 3
WRITE_RESPONSE = 4
READ_COMMAND = 5
READ_RESPONSE = 6
JUMP_COMMAND = 7
JUMP_RESPONSE = 8
SET_FIRMWARE_BROKEN_COMMAND = 9
SET_FIRMWARE_BROKEN_RESPONSE = 10
RESET_COMMAND = 100
RESET_RESPONSE = 101

# Memory area definitions
BOOTLOADER_MEMORY = 0
FIRMWARE_MEMORY = 1

# Number of 32 bit values contained in 1 data block
DATA_BLOCK_SIZE = 128


class CommunicationError(Exception):
    """Exception raised when the hardware does not respond or the received response is not as
    expected.
    """
    pass


class InvalidOperationError(Exception):
    """Exception raised when an operation is requested that can not be executed."""
    pass


class VerificationFailedError(Exception):
    """Exception raised when the verification of the flashed firmware failed."""
    pass


class FirmwareFlasher:
    """Class to flash firmware and bootloader binaries to boards running the kiteOS firmware."""
    def __init__(self, puli_port, puli_ttl, interface_address, cb_info,
                 cb_progress):
        """

        :param puli_port:         Port to use for communication with the target board.
        :param puli_ttl:          TTL to use for communication with the target board.
        :param interface_address: Ip address of network interface that is used by pulicast.
        :param cb_info:           Callback called with info message. Uses print() if set to None.
        :param cb_progress:       Callback called with progress integer. Uses tqdm if set to None.
        """
        self.port = int(puli_port)
        self.ttl = int(puli_ttl)
        self.interface_address = interface_address
        # Store channel names and printout flag in private variables
        self._communication_channel = "iap"
        self.cb_info = cb_info
        self.cb_progress = cb_progress
        self.critical_error_occured = False

        # Initialize response queue
        self._response_queue = queue.Queue()

        self.node = ThreadedNode("kiteos-flashtool", port=self.port, ttl=self.ttl,
                                 interface_address=self.interface_address)
        self.node.start()

        self.node[self._communication_channel].subscribe(self._response_handler)

    def set_board_identifier(self, board_identifier):
        self.current_board_identifier = board_identifier

        # Convert unique board identifier string into binary representation
        if board_identifier == "Unknown_PCB":
            self._target_board_identifier = [0] * 23
        else:
            try:
                pcb_type = board_identifier.split("-")[0]
                if not 0 < len(pcb_type) <= 16:
                    raise Exception("Invalid PCB type")
                pcb_version = board_identifier.split("-")[1].split("#")[0]
                if not len(pcb_version.split(".")) == 3:
                    raise Exception("Invalid PCB version")
                pcb_id = int(board_identifier.split("-")[1].split("#")[1])
                if not 0 <= pcb_id <= 0xFFFFFFFF:
                    raise Exception("Invalid PCB ID")
                pcb_version_major = int(pcb_version.split(".")[0])
                pcb_version_minor = int(pcb_version.split(".")[1])
                pcb_version_schematic = int(pcb_version.split(".")[2])
                if (not 0 <= pcb_version_major <= 255) or (not 0 <= pcb_version_minor <= 255) or \
                        (not 0 <= pcb_version_schematic <= 255):
                    raise Exception("Invalid PCB version")
                self._target_board_identifier = [
                    *[ord(x) for x in pcb_type.ljust(16, '\0')],
                    pcb_version_major, pcb_version_minor, pcb_version_schematic,
                    *struct.unpack("4B", struct.pack(">I", pcb_id))]
            except:
                self._print_info_message("Invalid board ID")
                exit(1)

        pulicast_session_id = self.node.session_id
        pulicast_session_id_list = struct.unpack("8B", struct.pack(">q", pulicast_session_id))
        self._session_id = [0] * 15
        self._session_id += pulicast_session_id_list

    def get_available_pcbs(self):
        self._print_info_message("Detecting pcbs...")
        node_discoverer = NodeDiscoverer(self.node)
        pcb_names = []
        def on_new_node(node: NodeView):
            if (node.name not in pcb_names):
                if("iap" in node.subscriptions and "iap" in node.publications.keys() and "mem" in node.subscriptions):
                    pcb_names.append(node.name)
        node_discoverer.add_on_item_added_callback(on_new_node)

        time.sleep(3)
        self._print_info_message("Following pcbs found: " + str(pcb_names))
        return pcb_names

    def reset(self):
        self._send_command_await_response(
            self._communication_channel,
            [*self._session_id, *self._target_board_identifier, RESET_COMMAND],
            [*self._target_board_identifier, *self._session_id, RESET_RESPONSE])

    def flash_bootloader(self, binary_path):
        """Flashes a bootloader binary to the target board.

        :param binary_path: Path of the bootloader binary.
        :return: 0 on success, 1 otherwise.
        """
        # Read bootloader binary
        data_blocks = self._read_binary_from_file(binary_path)

        # Check if target is running bootloader or firmware
        state = self._await_state()
        if state is None:
            raise CommunicationError("Hardware is not responding.")
        elif state == BOOTLOADER_MEMORY:
            raise InvalidOperationError(
                "Cannot flash bootloader. Target is running in bootloader mode.")

        # Erase bootloader
        self._print_info_message("Erasing bootloader...")
        self._erase_memory(BOOTLOADER_MEMORY)

        # Flash new bootloader
        iterator = range(len(data_blocks))
        if not self.cb_progress:
            iterator = tqdm.tqdm(iterator, desc="Flashing bootloader", unit='Blocks',
                                 mininterval=0.02, miniters=0)
        else:
            self._print_info_message("Flashing bootloader...")

        for i in iterator:
            self._call_progress_callback(i, len(data_blocks))
            self._write_data_block(BOOTLOADER_MEMORY, i, data_blocks[i])

        # Verify flashed bootloader
        iterator = range(len(data_blocks))
        if not self.cb_progress:
            iterator = tqdm.tqdm(iterator, desc="Verifying bootloader", unit='Blocks',
                                 mininterval=0.02, miniters=0)
        else:
            self._print_info_message("Verifying bootloader...")

        for i in iterator:
            self._call_progress_callback(i, len(data_blocks))
            if self._read_data_block(BOOTLOADER_MEMORY, i) != data_blocks[i]:
                raise VerificationFailedError(
                    "Verification of memory block \"{}\" failed.".format(i))

        # Flashing successful
        self._print_info_message("Bootloader update successful.")

    def flash_firmware(self, binary_path):
        """Flashs a kiteOS firmware binary to the target board.

        :param binary_path: Path of the firmware binary.
        :return: 0 on success, 1 otherwise.
        """
        self._print_info_message("Flashing pcb: " + self.current_board_identifier)

        # Read firmware binary
        data_blocks = self._read_binary_from_file(binary_path)

        # Check if target is running bootloader or firmware
        state = self._await_state()
        if state is None:
            raise CommunicationError("Hardware is not responding.")

        elif state == FIRMWARE_MEMORY:
            # Jump to bootloader
            self._print_info_message("Starting bootloader...")
            self._jump(BOOTLOADER_MEMORY)
            if self._await_state(20.0) is not BOOTLOADER_MEMORY:
                raise CommunicationError("Bootloader is not responding.")

        # Invalidate firmware
        self._set_firmware_broken(1)

        # Erase firmware
        self._print_info_message("Erasing firmware...")
        self._erase_memory(FIRMWARE_MEMORY)

        # Flash new firmware
        iterator = range(len(data_blocks))
        if not self.cb_progress:
            iterator = tqdm.tqdm(iterator, desc="Flashing firmware", unit='Blocks',
                                 mininterval=0.02, miniters=0)
        else:
            self._print_info_message("Flashing firmware...")

        for i in iterator:
            self._call_progress_callback(i, len(data_blocks))
            self._write_data_block(FIRMWARE_MEMORY, i, data_blocks[i])

        # Verify flashed firmware
        iterator = range(len(data_blocks))
        if not self.cb_progress:
            iterator = tqdm.tqdm(iterator, desc="Verifying firmware", unit='Blocks',
                                 mininterval=0.02, miniters=0)
        else:
            self._print_info_message("Verifying firmware...")

        for i in iterator:
            self._call_progress_callback(i, len(data_blocks))
            if self._read_data_block(FIRMWARE_MEMORY, i) != data_blocks[i]:
                raise VerificationFailedError(
                    "Verification of memory block \"{}\" failed.".format(i))

        # Validate firmware
        self._set_firmware_broken(0)

        # Jump to firmware
        self._print_info_message("Starting firmware...")
        self._jump(FIRMWARE_MEMORY)
        if self._await_state(20.0) is not FIRMWARE_MEMORY:
            raise CommunicationError("Firmware is not responding.")

        # Flashing successful
        self._print_info_message("Firmware update successful.")

    def _print_info_message(self, message):
        """Prints an info message to the console or calls info callback if not set to None.

        :param message: Message to print.
        """
        if self.cb_info:
            self.cb_info(message)
        else:
            print(message)

    def _call_progress_callback(self, progress, max):
        """Calls progress callback if not set to None.

        :param progress: progress as integer.
        :param max:      maximum progress as integer.
        """
        if self.cb_progress:
            self.cb_progress(progress, max)

    def _erase_memory(self, memory_area):
        """Erases a memory area.

        :param memory_area: Memory area to erase. Must be one of the memory area definitions above.
        """
        # Erase memory area
        if memory_area == BOOTLOADER_MEMORY:
            self._send_command_await_response(
                self._communication_channel,
                [*self._session_id, *self._target_board_identifier, ERASE_COMMAND, memory_area],
                [*self._target_board_identifier, *self._session_id, ERASE_RESPONSE, memory_area,
                 0x00], timeout=5.0)
        if memory_area == FIRMWARE_MEMORY:
            self._send_command_await_response(
                self._communication_channel,
                [*self._session_id, *self._target_board_identifier, ERASE_COMMAND, memory_area],
                [*self._target_board_identifier, *self._session_id, ERASE_RESPONSE, memory_area,
                 0x00], timeout=15.0)

    def _write_data_block(self, memory_area, data_block_index, data_block):
        """Writes a data block to a memory area.

        :param memory_area:      Memory area to write to. Must be one of the memory area definitions
                                 above.
        :param data_block_index: Index of the data block to write. Must be a 16 but unsigned integer
                                 value.
        :param data_block:       Data block to write. Must be a list of 8 bit unsigned integer
                                 values.
        """
        # Split data block index integer into bytes
        data_block_index_list = [ctypes.c_uint8((data_block_index & 0x0000ff00) >> 8).value,
                                 ctypes.c_uint8(data_block_index & 0x000000ff).value]

        # Write data block
        self._send_command_await_response(
            self._communication_channel,
            [*self._session_id, *self._target_board_identifier, WRITE_COMMAND, memory_area,
             *data_block_index_list, *data_block],
            [*self._target_board_identifier, *self._session_id, WRITE_RESPONSE, memory_area,
             *data_block_index_list, 0x00])

    def _read_data_block(self, memory_area, data_block_index):
        """Reads data from a memory area.

        :param memory_area:      Memory area to read from. Must be one of the memory area
                                 definitions above.
        :param data_block_index: Index of the data block to read. Must be a 16 but unsigned integer
                                 value.
        :return: Returns read data block as a list of 8 bit unsigned integer values.
        """
        # Split data block index integer into bytes
        data_block_index_list = [ctypes.c_uint8((data_block_index & 0x0000ff00) >> 8).value,
                                 ctypes.c_uint8(data_block_index & 0x000000ff).value]

        # Read data block
        response = self._send_command_await_response(
            self._communication_channel,
            [*self._session_id, *self._target_board_identifier, READ_COMMAND, memory_area,
             *data_block_index_list],
            [*self._target_board_identifier, *self._session_id, READ_RESPONSE, memory_area,
             *data_block_index_list])

        # Return part of the response message which represents the data block
        return response[50:]

    def _jump(self, memory_area):
        """Executes a jump to a a memory area.

        :param memory_area: Memory area to jump to. Must be one of the memory area definitions
                            above.
        """
        # Jump to memory area
        self._send_command_await_response(self._communication_channel,
                                          [*self._session_id, *self._target_board_identifier,
                                           JUMP_COMMAND, memory_area],
                                          [*self._target_board_identifier, *self._session_id,
                                           JUMP_RESPONSE, memory_area, 0x00])

    def _set_firmware_broken(self, firmware_broken):
        """Tells the device to write the funcrional state of the firmware to the persistent memory.

        :param  firmware_broken: 0 if the firmware is considered as non functional, 1 - 255 if not.
        """
        # Jump to memory area
        self._send_command_await_response(self._communication_channel,
                                          [*self._session_id, *self._target_board_identifier,
                                           SET_FIRMWARE_BROKEN_COMMAND, firmware_broken],
                                          [*self._target_board_identifier, *self._session_id,
                                           SET_FIRMWARE_BROKEN_RESPONSE, firmware_broken])

    def _await_state(self, timeout=3.5):
        response = self._await_response([*self._target_board_identifier, *([0xFF] * 23), STATUS],
                                        timeout)
        if not response:
            return None
        return response[47]

    def _send_command_await_response(self, channel, command, expected_response, timeout=1.0,
                                     max_resends=5):
        """Sends an in application programmer command to the given pulicast channel and waits for
        the response.

        :param channel:           pulicast channel to send the command to. Must be a string.
        :param command:           In application programmer command to send. Must be a list of 8 bit
                                  unsigned integer values.
        :param expected_response: Partial message which must be the beginning of the response.
                                  Messages not starting with this values are rejected. Must be a
                                  list of 8 bit unsigned integer values.
        :param timeout:           Timeout in seconds.
        :param max_resends:       Maximum number of resends on timeout.
        :return: Returns received response.
        """
        # Send command
        self._send_command(channel, command)
        response = self._await_response(expected_response, timeout)

        # Resend if necessary
        while response is None:
            if max_resends > 0:
                # Resend command
                max_resends -= 1
                self._send_command(channel, command)
                response = self._await_response(expected_response, timeout)
            else:
                # Maximum number of resends reached
                raise CommunicationError("Target not responding to command \"{}\".".format(command))

        return response

    def _await_response(self, expected_response, timeout=1.0):
        """Waits for a response from the in application programmer.

        :param expected_response: Partial message which must be the beginning of the response.
                                  Messages not starting with this values are rejected. Must be a
                                  list of 8 bit unsigned integer values.
        :param timeout:           Maximum time to wait for a response.
        :return: Returns received response on success and "None" on timeout.
        """
        # Store start time
        start_time = time.time()

        # Await response
        while time.time() - start_time < timeout:
            if not self._response_queue.empty():  # Message received
                received_message = self._response_queue.get()
                # Check if the message is the expected response message
                if len(received_message) >= len(expected_response):
                    if received_message[:len(expected_response)] == expected_response:
                        # Return response
                        return received_message
            time.sleep(0.001)

        # Timeout reached
        return None

    def _send_command(self, channel, command):
        """Sends an in application programmer command to the given pulicast channel.

        :param channel: Pulicast channel to send the command to. Must be a string.
        :param command: In application programmer command to send. Must be a list of 8 bit unsigned
                        integer values.
        """
        # Convert command to uint8 C type
        command_int8 = []
        for element in command:
            command_int8.append(ctypes.c_uint8(element).value)

        # Assemble message
        message = zcom.timestamped_vector_byte()
        message.values = command_int8
        message.len = len(command)

        # Send command
        self.node[channel] << message

    def _response_handler(self, msg: zcom.timestamped_vector_byte, source: Address, lead: int):
        """Handler called when a pulicast message is received."""
        # Write received message to response queue
        self._response_queue.put(list(msg.values))

    @staticmethod
    def _read_binary_from_file(path):
        """Reads a binary file from a given path.

        :param path: Path where the binary is located.
        :return: Returns a list of data blocks. Each block is a list of 8-bit integer values.
        """
        block_list = []

        # Open binary file to read
        try:
            file = open(path, "rb")
        except FileNotFoundError:
            raise FileNotFoundError("Failed to open file \"{}\".".format(path))

        # Read file in blocks of DATA_BLOCK_SIZE * 4 bytes
        while True:
            # Read data block from binary
            bytes_read = file.read(DATA_BLOCK_SIZE * 4)

            if len(bytes_read) == 0:
                # Reading finished, return data
                return block_list
            elif len(bytes_read) < DATA_BLOCK_SIZE * 4:
                # Do zero padding if less than 32 bytes were available
                bytes_read = bytes_read.ljust(DATA_BLOCK_SIZE * 4, b"\0")

            # append data block as a list of 8 32-bit integer values to block list
            block_list.append(list(struct.unpack("<" + str(DATA_BLOCK_SIZE * 4) + "B", bytes_read)))


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description="Update firmware of a KiteOS hardware component.")
    parser.add_argument("board_id",
                        help="Target board unique hardware identifier. Use <all> to flash all available pcbs.")
    parser.add_argument("file_path", help="Path of the binary to flash.")
    parser.add_argument("-b", "--bootloader", action="store_true",
                        help="Updates the bootloader instead of the firmware.")
    parser.add_argument("-p", "--puli-port", default=pulicast.DEFAULT_PORT,
                        help="The pulicast port to publish on.")
    parser.add_argument("-t", "--puli-ttl", default=1,
                        help="The pulicast ttl to publish with.")
    parser.add_argument("-i", "--interface", default="0.0.0.0", type=str,
                        help="Pulicast capture interface ip address")
    args = parser.parse_args()

    # Flash firmware or bootloader using the firmware flasher class
    firmware_flasher = FirmwareFlasher(args.puli_port, args.puli_ttl, args.interface, None, None)

    if (args.board_id == 'all'):
        pcb_names = firmware_flasher.get_available_pcbs()
    else:
        pcb_names = [args.board_id]

    for board_identifier in pcb_names:
        firmware_flasher.set_board_identifier(board_identifier)

        try:
            if args.bootloader:
                firmware_flasher.flash_bootloader(args.file_path)
            else:
                firmware_flasher.flash_firmware(args.file_path)
        except (FileNotFoundError, CommunicationError, InvalidOperationError,
                VerificationFailedError) as e:
            print(e)
