# Changelog

Until there is a dedicated guide how to use this log, please stick to this article: https://keepachangelog.com/de/0.3.0/
Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Fixed`, `Removed`

Before every entry put one of these to mark the severity of the change:
`Major`, `Minor` or `Patch`


## [Unreleased development](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v1.4.1...master)
## [1.4.1](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v1.4.0...1.4.1) - 23.07.2021
### Changed
- [Patch] Changed pipeline for open sourcing.

## [1.4.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v1.4.0...1.3.0) - 14.06.2021
### Added
- [Minor] Added reset tool.

## [1.3.0]
### Added
- [Patch] Added merge request template.
- [Patch] Added all option to flash all available pcbs.

### Removed
- [Minor] Removed in application programmer protocol documentation, which is now located in the KiteOS documentation.


## [1.2.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v1.1.0...v1.2.0) - 22.03.2021
### Added
- [Minor] Firmware flashing process is now writing the functional state of the firmware to the devices persistent memory.
- [Patch] Added address fields to status message.

### Changed
- [Minor] Firmware can now also be flashed when the device is running in bootloader mode.
- [Minor] Error handling is now done using exceptions.
- [Patch] Increased write/read data block size to 128 to speed up the flash process.


## [1.1.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v1.0.0...v1.1.0) - 04.03.2021
### Added
- [Minor] Added docu page.


## [1.0.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.4.2...v1.0.0) - 04.03.2021
### Changed
- [Major] The target board is now identified by using the unique hardware identifier instead of the board name.


## [0.4.2](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.4.1...v0.4.2) - 12.02.2021
### Changed
- [Patch] Increased timeout for starting firmware.


## [0.4.1](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.4.0...v0.4.1) - 12.02.2021
### Changed
- [Patch] Increased timeout for switching to bootloader code.


## [0.4.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.3.0...v0.4.0) - 05.02.2021

### Changed
- [Minor] Removed machine_readable and replaced it with cb_info,cb_error,cb_progress.

### Added
- [Patch] Added README.md


## [0.3.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.2.1...v0.3.0) - 14.12.2020
### Changed
- [Minor] Changed pulicast version to 1.0


## [0.2.1](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.2.0...v0.2.1) - 11.11.2020
### Changed
- [Patch] Changed default port to pulicast default.


## [0.2.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/v0.1.5...v0.2.0) - 27.10.2020
### Added
- [Minor] Added command line argument for pulicast network interface '-i/--interface'.

### Changed
- [Minor] Renamed program interface trigger from '-i/--program-interface-output' to '-m/--machine-readable'.
- [Minor] Replaced f/b commandline argument by -b commandline flag.

### Fixed
- [Patch] Pulicast has renamed ints SockerNode to ThreadedNode. This patch reflects this change.


## [0.1.5](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/0.1.4...v0.1.5) - 17.07.2020
### Added
- [Patch] Kiteos_flashtool released via Kiteswarms pip.


## [0.1.4](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/0.1.3...0.1.4) - 22.06.2020
### Changed
- [Patch] Not using trio framework anymore.


## [0.1.3](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/0.1.2...0.1.3) - 22.06.2020
### Fixed
- [Patch] Failing subscription.


## [0.1.2](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/0.1.1...0.1.2) - 22.06.2020
### Removed
- [Patch] Remove deprecated import of transport parameters.


## [0.1.1](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/compare/0.1.0...0.1.1) - 28.05.2020
### Fixed
- [Patch] Fixed entry point of installed script.


## [0.1.0](https://code.kiteswarms.com/kiteswarms/kiteos-flashtool/-/tree/0.1.0) - 28.05.2020
### Added
- [Minor] Basic flashtool functionality based on pulicast communication.
